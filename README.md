dev envirenment
setup 
- laragon https://laragon.org/
- nodejs https://nodejs.org/en/
- yarn https://classic.yarnpkg.com/fr/docs/install/#windows-stable
- vscode https://code.visualstudio.com/
- composer https://getcomposer.org/download/

step :1 change .env configuration file
step 2: add auth system 
https://laravel.com/docs/7.x/authentication#introduction
composer require laravel/ui
php artisan ui vue --auth
yarn install && yarn run dev
php artisan migrate:fresh --seed


--now you can test the authentification system--
step3:
php artisan make:model Product -a
--  -a option create model + factory + seed + migration--

step 4 : php artisan migrate:fresh --seed
step 5 : php artisan make:controller ProductController -r --model=Product


step6: list of all products

add this line in web.php
Route::resource('products','ProductController');
check new created routes by taping php artisan route:list

add this code in the function index in ProductionController.php

$products = Product::latest()->paginate(5);
return view('products.index',compact('products'));

step 7:     
add this code in the create function to all the new form

public function create()
{
    return view('products.create');
}


