<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'customer']);

        $admin = factory(\App\User::class)->create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
        ]);
        $admin->assignRole('admin');

        $customer = factory(\App\User::class)->create([
            'name' => 'customer',
            'email' => 'customer@gmail.com',
        ]);
        $customer->assignRole('customer');
    }
}
