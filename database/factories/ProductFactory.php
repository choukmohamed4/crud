<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'slug'=> $faker->name,
        'price'=> $faker->randomNumber(),
        'description'=> $faker->paragraph(),
        'image'=> $faker->imageUrl(),
        'category_id' => function () {
            return App\Category::inRandomOrder()->first()->id;
        }
    ];
});
