<?php

namespace App\Http\Controllers;

use App\Product,App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(12);
        return view('products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:20|min:5',
            'price' => 'required|numeric',
            'description' => 'required|min:15',
            'image' => 'required|image'

        ]);
        if ($request->image) {
            $path = "products/";
            $file_path =$path . Str::slug($request->name) . '.jpg';
            $image = \Image::make($request->image)->fit(400, 255)->encode('jpg');
            $path = \Storage::disk('public')->put($file_path, (string) $image);
        }

        Product::create([
            'name' => $request->name,
            'slug' => Str::slug($request->get('name')),
            'price' => $request->price,
            'description' => $request->description,
            'image' => url('/storage/' . $file_path),
            'category_id' => $request->category_id,
        ]);

        return redirect()->route('products.index')->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('products.edit',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'price' => 'required',
        ]);

        //upload image
        if ($request->image) {
            $path = "products/";
            $file_path =$path . Str::slug($request->name) . '.jpg';
            $image = \Image::make($request->image)->fit(400, 255)->encode('jpg');
            $path = \Storage::disk('public')->put($file_path, (string) $image);

            $product->update([
                'name' => $request->name,
                'slug' => Str::slug($request->get('name')),
                'price' => $request->price,
                'description' => $request->description,
                'image' => url('/storage/' . $file_path),
                'category_id' => $request->category_id,
            ]);

            return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
        }

        //update
        $product->update([
            'name' => $request->name,
            'slug' => Str::slug($request->get('name')),
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->category_id,
        ]);

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
}
