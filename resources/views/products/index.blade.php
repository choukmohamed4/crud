@extends('adminlte::page')

@section('title', 'List of all products')

@section('content_header')
    <h1>List of all products</h1>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>product</h2>
            </div>
            <div class="pull-right">
                @role('admin')
                    <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
                @else
                <a class="btn btn-success" href="#"> Imprimer</a>
                @endrole
            </div>
        </div>
    </div>
    <table class="table table-bordered my-4">
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Category</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->category->name }}</td>
            <td>
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>

                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete</button>
            </td>
        </tr>
        @endforeach
    </table>
    {!! $products->links() !!}
@stop
